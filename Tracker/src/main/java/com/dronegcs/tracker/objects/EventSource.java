package com.dronegcs.tracker.objects;

public enum EventSource {

    DB_SERVER,
    SYSTEM,
    DRONE,

}
